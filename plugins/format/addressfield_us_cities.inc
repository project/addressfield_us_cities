<?php

/**
 * @file
 * Autocomplete US cities.
 */

$plugin = array(
  'title' => t('City list for USA'),
  'format callback' => 'addressfield_us_cities_format_address',
  'type' => 'address',
  'weight' => -100,
);

/**
 * Format callback for Viet Nam address.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_us_cities_format_address(&$format, $address, $context = array()) {
  if ($address['country'] == 'US' && $context['mode'] == 'form') {
    $format['locality_block']['administrative_area']['#wrapper_id'] = $format['#wrapper_id'];
    $format['locality_block']['administrative_area']['#process'][] = 'ajax_process_form';
    $format['locality_block']['administrative_area']['#process'][] = 'addressfield_format_address_us_cities_process';
    $format['locality_block']['administrative_area']['#weight'] = -1;

    $format['locality_block']['administrative_area']['#ajax'] = array(
      'callback' => 'addressfield_standard_widget_refresh',
      'wrapper' => $format['#wrapper_id'],
    );
    $format['locality_block']['locality']['#autocomplete_path'] = 'addressfield-us-cities/autocomplete/'. $address['administrative_area'];
    $format['locality_block']['locality']['#element_validate'] = array('address_us_cities_validate_city');
  }
  else {
    if (isset($format['locality_block']['administrative_area'])) {
      // Cancel the AJAX for forms we don't control.
      $format['locality_block']['administrative_area']['#ajax'] = array();
    }
  }
}

function addressfield_format_address_us_cities_process($element) {
  $element['#limit_validation_errors'] = array($element['#parents']);

  return $element;
}


function address_us_cities_validate_city($element, &$form_state, &$form) {
  if (!empty($element['#value'])) {
    if (!empty($form_state['values']['commerce_customer_address']['und'][0]['administrative_area'])
      && !empty($form_state['values']['commerce_customer_address']['und'][0]['locality'])
    ) {
      $state = $form_state['values']['commerce_customer_address']['und'][0]['administrative_area'];
      $city = $form_state['values']['commerce_customer_address']['und'][0]['locality'];
      $cities = _addressfield_us_cities_query($state, $city, 'not like')->fetchAll();
        watchdog('cit', print_r($cities, true));
      if (empty($cities)) {
        form_error($element, t('Sorry, city does not match state.'));
        return FALSE;
      }
    }
  }
}


